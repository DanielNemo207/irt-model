# IRT model

## Name
Applying Item Response Theory on Estimating students CEFR

## Description
This is a self-project on estimating students' proficiency level (CEFR levels) based on performance on exercises related to finite set of concepts/skills

## Installation
pip install -r requirements.txt
